﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Planet
{
    class Command : ICommand
    {
        readonly Action<object> _execute;
        readonly Predicate<object> _canExecute;
        private Action<object> killProcess;

        public Command(Action<Object> execute, Predicate<object> canExecute)
        {
            this._execute = execute;
            _canExecute = canExecute;
        }

        public Command(Action<object> execute) : this(execute, null) { }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }    
            remove { CommandManager.RequerySuggested -= value; }
        }


        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }
    }
}
