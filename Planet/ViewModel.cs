﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Data;
using System.Windows.Threading;

namespace Planet
{
    class ViewModel
    {
        public ObservableCollection<Process> Processes { get; set; }

        public Command stopProcess {get; private set;}

        private DispatcherTimer refreshTimer;

        public ViewModel()
        {
            Processes = InitializeProcessList();
            stopProcess = new Command(StopProcess);
            InitializeTimer();
            CollectionView view = CollectionViewSource.GetDefaultView(Processes) as CollectionView;
        }

        private void InitializeTimer()
        {
            refreshTimer = new DispatcherTimer();
            refreshTimer.Tick += new EventHandler(onTimedEvent);
            refreshTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            refreshTimer.Start();
        }

        private void onTimedEvent(object sender, EventArgs e)
        {
            RefreshProcesses();
        }

        private void RefreshProcesses()
        {
            var newProcesses = Process.GetProcesses().ToList();

            var processesToRemove = Processes
                .Where((p) => !newProcesses.Exists((p2)=>p.Id == p2.Id))
                .ToList();
            var processesToAdd = newProcesses
                .Where((p) => !Processes.ToList().Exists((p2) => p.Id == p2.Id))
                .ToList();

            processesToRemove.ForEach((p) => Processes.Remove(p));
            processesToAdd.ForEach((p) => Processes.Add(p));
        }

        private ObservableCollection<Process> InitializeProcessList()
        {
            var list = Process.GetProcesses().ToList();
            return new ObservableCollection<Process>(list);
        }

        public void StopProcess(object arg)
        {
            var process = (Process)arg;
            process.Kill();
        }
    }
}
