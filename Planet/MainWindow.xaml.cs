﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Planet
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ViewModel();
            CollectionView view = CollectionViewSource.GetDefaultView(processListBox.ItemsSource) as CollectionView;
            view.Filter = CustomFilter;
            view.SortDescriptions.Add(new SortDescription((string)((ComboBoxItem)comboBox.SelectedValue).Content,ListSortDirection.Descending));
        }

        private bool CustomFilter(object obj)
        {
            if (string.IsNullOrEmpty(searchTextBox.Text))
            {
                return true;
            }
            else
            {
                return (obj.ToString().IndexOf(searchTextBox.Text, StringComparison.OrdinalIgnoreCase) >= 0);
            }
        }

        private void OnSearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(processListBox.ItemsSource).Refresh();
        }

        private void OnComboBox_SelectionChanged(object sender, EventArgs e)
        {
            CollectionView view = CollectionViewSource.GetDefaultView(processListBox.ItemsSource) as CollectionView;
            view.SortDescriptions.Clear();
            view.SortDescriptions.Add(new SortDescription((string)((ComboBoxItem)comboBox.SelectedValue).Content, ListSortDirection.Descending));
        }
    }
}
